# liebessen

## Install
```bash
npm install -g nodemon
npm install -g grunt
npm install -g bower
npm install
npm install --no-bin-links # only for Windows
```

## Run in development
```bash
npm run dev
```

## Run in production
```bash
npm start
```