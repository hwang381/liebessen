var express = require('express');
var router = express.Router();
var User = require('../../models/user-model');
var passport = require('passport');

router.post('/signup', function (req, res) {
    User.register(
        new User({
            name: req.body.name,
            email: req.body.email
        }), req.body.password, function (err, user) {
            if (err) {
                res.internalError(err);
            } else {
                passport.authenticate('local')(req, res, function () {
                    res.success({});
                });
            }
        });
});

router.post('/login', function (req, res, next) {
    passport.authenticate('local', function (err, user, info) {
        console.log(err, user, info);
        if (err) {
            res.internalError(err);
        } else if (!user) {
            res.internalError(info);
        } else {
            req.logIn(user, function (err) {
                if (err) {
                    res.internalError(err);
                } else {
                    res.success({});
                }
            });
        }
    })(req, res, next);
});

router.get('/logout', function (req, res) {
    req.logout();
    res.success({});
});

router.get('/status', function (req, res) {
    if (req.isAuthenticated()) {
        res.success({});
    } else {
        res.notAuthorized();
    }
});

router.get('/', function (req, res) {
    if (req.isAuthenticated()) {
        res.success(req.user);
    } else {
        res.notAuthorized();
    }
});

router.put('/', function (req, res) {
    if (req.isAuthenticated()) {
        User.update({email: req.user.email}, req.body, null, function (err, rowsAffected) {
            if (err) {
                res.internalError(err);
            } else {
                res.success({});
            }
        });
    } else {
        res.notAuthorized();
    }
});

module.exports = router;