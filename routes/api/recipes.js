var express = require('express');
var router = express.Router();
var request = require('request');
var Recipe = require('../../models/recipe-model');
var IngredientsToRecipes = require('../../models/ingredients-to-recipes-model');

var apiKey = 'e13991b65d6cce322896341218cbb418';

router.get('/', function (req, res) {
    if (req.isAuthenticated()) {
        var ingredients = req.query['ingredients'];

        // try to find in cache
        IngredientsToRecipes.findOne({'ingredients': ingredients}, function (err, result) {
            if (err) {
                res.internalError(err);
            } else if (!result) {
                // recipes not found, request, cache and respond

                // build query string for food2fork
                var qString = '';
                for (var i = 0 ; i < ingredients.length ; ++i) {
                    qString += ingredients[i];
                    if (i !== ingredients.length - 1) {
                        qString += ',';
                    }
                }

                // make api call to food2fork
                var url = 'http://food2fork.com/api/search?key=' + apiKey + '&q=' + qString;
                request(url, function (err, response, body) {
                    if (!err && response.statusCode === 200) {
                        // cache
                        var recipes = JSON.parse(body)['recipes'];
                        new IngredientsToRecipes({
                            'ingredients': ingredients,
                            'recipes': recipes
                        }).save();

                        // respond
                        res.success(recipes);
                    } else {
                        res.internalError(err);
                    }
                });
            } else {
                // recipes found and respond
                res.success(result['recipes']);
            }
        });


    } else {
        res.notAuthorized();
    }
});

router.get('/:id', function (req, res) {
    if (req.isAuthenticated()) {
        var recipe_id = req.params['id'];

        // try to find in cache
        Recipe.findOne({'recipe_id': recipe_id}, function (err, recipe) {
            if (err) {
                res.internalError(err);
            } else if (!recipe) {
                // recipe not found, request, cache and respond
                var url = 'http://food2fork.com/api/get?key=' + apiKey + '&rId=' + recipe_id;
                request(url, function (err, response, body) {
                    if (!err && response.statusCode === 200) {
                        // cache
                        var recipe = JSON.parse(body)['recipe'];
                        new Recipe(recipe).save();

                        // respond
                        res.success(recipe);
                    } else {
                        res.internalError(err);
                    }
                })
            } else {
                // recipe found and respond
                res.success(recipe);
            }
        });

    } else {
        res.notAuthorized();
    }
});

module.exports = router;