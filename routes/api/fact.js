var express = require('express');
var router = express.Router();
var parse = require('csv-parse');
var fs = require('fs');

// read csv file
fs.readFile(__dirname + '/../../facts.csv', function (err, data) {
    if (err) {
        throw err;
    } else {
        // parse csv file
        parse(data, function (err, raw_table) {
            if (err) {
                throw err;
            } else {
                // extract header and table
                var header = raw_table[0];
                var table = raw_table.slice(1, raw_table.length);

                // extract keywords array
                var keywords_array = table.map(function (row) {
                    return row[0];
                });

                // construct keywords matrix
                var keywords_matrix = [];
                var finish = false;
                var index = 0;

                while (!finish) {
                    var tempArray = [];
                    var shallFinish = true;

                    for (var i = 0 ; i < keywords_array.length ; ++i) {
                        var keyword = keywords_array[i];
                        var char = keyword[index];
                        if (char) {
                            shallFinish = false;
                        }
                        tempArray.push(char);
                    }

                    keywords_matrix.push(tempArray);
                    finish = shallFinish;
                    index++;
                }
                
                // setup route
                router.get('/:name', function (req, res) {
                    if (req.isAuthenticated()) {
                        res.success({
                            header: header,
                            values: search(req.params['name'])
                        });
                    } else {
                        res.notAuthorized();
                    }
                });

                // search function
                function search(keyword) {
                    keyword = keyword.toUpperCase();

                    var start = 0;
                    var end = table.length - 1;

                    for (var pos = 0 ; pos < keyword.length ; ++pos) {
                        var char = keyword[pos];

                        var array = keywords_matrix[pos];
                        if (!array) {
                            break;
                        } else {
                            function indexOfFirstGreaterOrEqual(a, target) {
                                var lo = 0;
                                var hi = a.length - 1;

                                while (lo <= hi) {
                                    if (lo === hi) {
                                        return lo;
                                    } else {
                                        var mid = lo + Math.floor((hi - lo) / 2);
                                        if (target <= a[mid]) {
                                            hi = mid;
                                        } else {
                                            lo = mid + 1;
                                        }
                                    }
                                }

                                return -1;
                            }

                            // cut local array
                            var localArray = array.slice(start, end + 1);

                            // find local bounds
                            var localStart = indexOfFirstGreaterOrEqual(localArray, char);
                            var nextChar = String.fromCharCode(char.charCodeAt(0) + 1);
                            var localEnd = indexOfFirstGreaterOrEqual(localArray, nextChar);
                            if (char !== localArray[localEnd]) {
                                localEnd--;
                            }

                            // convert to global bounds
                            start = start + localStart;
                            end = end - (localArray.length - 1 - localEnd);
                        }
                    }

                    return table.slice(start, end + 1);
                }
            }
        })
    }
});

module.exports = router;