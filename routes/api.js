var express = require('express');
var router = express.Router();

router.use('/users', require('./api/users'));
router.use('/recipes', require('./api/recipes'));
router.use('/fact', require('./api/fact'));

router.options(function(req, res) {
    res.writeHead(200).end();
});

module.exports = router;