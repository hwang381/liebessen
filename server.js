var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var mongoose = require('mongoose');
var passport = require('passport');
var User = require('./models/user-model');
var app = express();

// constants
var constants = require('./configs/constants');
var dev = !(process.argv.length === 3 && process.argv[2] === 'prod');

// db
var mongo_url = dev ? constants.mongo_url.dev : constants.mongo_url.prod;
mongoose.connect(mongo_url);
console.log('mongo_url: ' + mongo_url);

// views
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// favicon
//app.use(favicon(path.join(__dirname, 'frontend', 'favicon.ico')));

// logger
app.use(logger('dev'));

// body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// cookie parser
app.use(cookieParser());

// session
app.use(session({
    secret: 'this is not the cookie secret for liebessen'
}));

// static
app.use(express.static(path.join(__dirname, 'frontend')));

// CORS
var allowCrossDomain = function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization");
    next();
};
app.use(allowCrossDomain);

// passport
app.use(passport.initialize());
app.use(passport.session());

// configure passport
passport.use(User.createStrategy());
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// response writer middleware
app.use(function (req, res, next) {
    res.success = function (data) {
        res.status(200).json({
            data: data
        });
    };

    res.internalError = function (err) {
        res.status(500).json({
            err: err ? err : 'Internal error'
        });
    };

    res.notAuthorized = function (err) {
        res.status(401).json({
            err: err ? err : 'Not authorized'
        });
    };

    next();
});

// routes
app.use('/', require('./routes/index'));
app.use('/api', require('./routes/api'));

// error handlers
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
if (dev) {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
