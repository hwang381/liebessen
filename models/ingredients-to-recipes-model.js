var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ingredientToRecipesSchema = new Schema({
    ingredients: [String],
    recipes: [Schema.Types.Mixed]
});

module.exports = mongoose.model('IngredientsToRecipes', ingredientToRecipesSchema);