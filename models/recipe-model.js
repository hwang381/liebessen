var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var recipeSchema = new Schema({
    publisher: String,
    f2f_url: String,
    ingredients: [String],
    source_url: String,
    recipe_id: String,
    image_url: String,
    publisher_url: String,
    title: String
});

module.exports = mongoose.model('Recipe', recipeSchema);