var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var userSchema = new Schema({
    name: String,
    email: String,
    password: String,
    favoriteRecipes: [String],
    ingredients: [{
        name: String,
        quantity: Number,
        measurement: String,
        category: String,
        nutrition: Schema.Types.Mixed,
        expiresAt: Date
    }]
});

userSchema.plugin(passportLocalMongoose, {
    usernameField: 'email',
    errorMessages: {
        IncorrectPasswordError: 'Email or password are incorrect',
        IncorrectUsernameError: 'Email or password are incorrect',
        MissingUsernameError: 'No email was given',
        UserExistsError: 'A user with the given email is already registered'
    }
});

module.exports = mongoose.model('User', userSchema);