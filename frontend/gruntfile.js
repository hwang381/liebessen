module.exports = function (grunt) {
    grunt.initConfig({
        clean: {
            css: ['public/css'],
            js: ['public/js']
        },
        concat: {
            dev: {
                options: {
                    separator: ';'
                },
                files: {
                    'public/js/bundle.js': ['source_js/**/*.js']
                }
            }
        },
        uglify: {
            dev: {
                files: {
                    'public/js/bundle.min.js': ['public/js/bundle.js']
                }
            }
        },
        compass: {
            dev: {
                options: {
                    config: 'compass_config.rb'
                }
            }

        },
        watch: {
            options: {
                livereload: true
            },
            scripts: {
                files: ['source_js/**/*.js'],
                tasks: ['clean:js', 'concat:dev', 'uglify:dev']
            },
            sass: {
                files: ['source_sass/**/*.scss'],
                tasks: ['clean:css', 'compass:dev']
            },
            html: {
                files: ['index.html', 'public/**/*.html']
            }
        }
    });
    grunt.registerTask('init', ['concat:dev', 'uglify:dev', 'compass:dev']);
    grunt.registerTask('default', ['watch']);
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
};
