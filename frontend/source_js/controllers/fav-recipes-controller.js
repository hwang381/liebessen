angular.module('liebessen-controllers').controller('FavRecipesController', ['$scope', 'UserService', 'RecipeService', 'itemsPerPage', '$filter', function ($scope, UserService, RecipeService, itemsPerPage, $filter) {
    // bs3Alert
    (function () {
        $('#alert').bs3Alert({
            priority: 'success',
            titles: {
                success: '',
                danger: ''
            }
        });
    })();
    $scope.showSuccessAlert = function (msg) {
        $(document).trigger('show-alert', {
            priority: 'success',
            message: msg
        });
    };
    $scope.showFailureAlert = function (msg) {
        $(document).trigger('show-alert', {
            priority: 'danger',
            message: msg
        });
    };

    // loading
    $scope.loading = true;

    // empty
    $scope.empty = false;

    UserService.getFavoriteRecipes().then(function (favoriteRecipes) {
        $scope.loading = false;

        // if empty
        if (favoriteRecipes.length === 0) {
            $scope.empty = true;
        } else {
            $scope.recipes = favoriteRecipes;

            // pagination
            $scope.itemsPerPage = itemsPerPage;

            // (re)load total items and current page
            $scope.totalItems = $scope.recipes.length;
            $scope.currentPage = 1;

            // when page changed, reload paginated recipes
            $scope.pageChanged = function () {
                var start = ($scope.currentPage - 1) * $scope.itemsPerPage;
                var end = start + $scope.itemsPerPage;
                if (end >= $scope.recipes.length) {
                    end = $scope.recipes.length;
                }
                $scope.paginatedRecipes = $scope.recipes.slice(start, end);
            };

            // trigger paginated recipes
            $scope.pageChanged();
        }
    }, function (err) {
        $scope.loading = false;
        $scope.showFailureAlert(err);
    })
}]);