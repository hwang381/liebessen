angular.module('liebessen-controllers').controller('IngredientController', ['$scope', '$routeParams', '$location', 'UserService', 'RecipeService', function ($scope, $routeParams, $location, UserService, RecipeService) {
    console.log('Hello from IngredientController');
    $scope.currentIngredientId = $routeParams.id;

    // bs3Alert
    (function () {
        $('#alert').bs3Alert({
            priority: 'success',
            titles: {
                success: '',
                danger: ''
            }
        });
    })();
    $scope.showSuccessAlert = function (msg) {
        $(document).trigger('show-alert', {
            priority: 'success',
            message: msg
        });
    };
    $scope.showFailureAlert = function (msg) {
        $(document).trigger('show-alert', {
            priority: 'danger',
            message: msg
        });
    };


    /*
        Edit the food by looking at changes to the edit/information box
     */
    $scope.editFood = function(foodData) {
        UserService.get().then(function (userData) {
            var curIngredient = $.grep($scope.userData.ingredients, function(elt){
                return elt._id === $scope.currentIngredientId;
            })[0];

            // since these are objects, can't user indexof
            // var index = userData.ingredients.indexOf(curIngredient);
            var index = -1;
            for (var i = 0; i < userData.ingredients.length; i++) {
                if ($scope.currentIngredientId === userData.ingredients[i]._id) {
                    index = i;
                }
            }

            curIngredient.quantity = foodData.quantity;
            userData.ingredients[index] = curIngredient;
            console.log(userData.ingredients);
            console.log(index);
            UserService.update({'ingredients' : userData.ingredients}).then(function() {
                $scope.showSuccessAlert("Edit was successful");
            }, function() {
                $scope.showFailureAlert("Edit failed");
            })
        })
    }

    $scope.deleteFood = function(foodData) {
        UserService.get().then(function (userData) {
            var curIngredient = $.grep($scope.userData.ingredients, function(elt){
                return elt._id === $scope.currentIngredientId;
            })[0];

            // since these are objects, can't user indexof
            // var index = userData.ingredients.indexOf(curIngredient);
            var index = -1;
            for (var i = 0; i < userData.ingredients.length; i++) {
                if ($scope.currentIngredientId === userData.ingredients[i]._id) {
                    index = i;
                }
            }
            var category = curIngredient.category;

            userData.ingredients.splice(index, 1);
            UserService.update({'ingredients' : userData.ingredients}).then(function() {
                // we should be redirected to the category page
                console.log('/food/' + category);
                $location.path('/food/' + category);
                // $scope.showSuccessAlert("Edit was successful");
            }, function() {
                $scope.showFailureAlert("Edit failed");
            })

        })
    }

    /*
     Retrieve the foods of the category we are in
     */
    $scope.getFoods = function() {
        UserService.get().then(function (userData) {
                // success
                $scope.userData = userData;
                // http://stackoverflow.com/questions/6930350/easiest-way-to-search-a-javascript-object-list-with-jquery
                $scope.matches = $.grep($scope.userData.ingredients, function(elt){
                    return elt._id === $scope.currentIngredientId;
                })[0];
                console.log($scope.matches);

            },
            function (userData) {
                // error
            });
    }
    $scope.getFoods();

    UserService.get().then(function (user) {
        // get user's current ingredients
        var ingredients = [$scope.matches];

        // filter out those already expired
        // todo

        // find recipes for them
        RecipeService.getMultiple(ingredients.map(function (i) {
            return [i['name']];
        })).then(function (recipes) {
            $scope.recipes = recipes;
        }, function (err) {
            console.error(err);
        })
    }, function (err) {
        console.error(err);
    })
}]);