angular.module('liebessen-controllers').controller('RecipesController', ['$scope', 'UserService', 'RecipeService', 'itemsPerPage', '$filter', function ($scope, UserService, RecipeService, itemsPerPage, $filter) {
    // bs3Alert
    (function () {
        $('#alert').bs3Alert({
            priority: 'success',
            titles: {
                success: '',
                danger: ''
            }
        });
    })();
    $scope.showSuccessAlert = function (msg) {
        $(document).trigger('show-alert', {
            priority: 'success',
            message: msg
        });
    };
    $scope.showFailureAlert = function (msg) {
        $(document).trigger('show-alert', {
            priority: 'danger',
            message: msg
        });
    };

    // loading
    $scope.loading = true;

    // empty
    $scope.empty = false;

    UserService.get().then(function (user) {
        // get user's all ingredients
        var ingredients = user['ingredients'];

        if (ingredients.length === 0) {
            // if empty
            $scope.loading = false;
            $scope.empty = true;
        } else {
            // sort by expiration data
            ingredients.sort(function (i, j) {
                return (new Date(i['expiresAt']).getTime()) > (new Date(j['expiresAt']).getTime())
            });

            // filter out those already expired
            // todo

            // find top three
            $scope.expiringIngredients = ingredients.slice(0, 3);

            // find recipes for them
            RecipeService.getMultiple($scope.expiringIngredients.map(function (i) {
                return [i['name']];
            })).then(function (recipes) {
                $scope.loading = false;
                $scope.recipes = recipes;
                $scope.filteredRecipes = [];
                $scope.pageChanged = function () {};

                // filtering
                $scope.expiringIngredients = $scope.expiringIngredients.map(function (ingredient) {
                    ingredient['selected'] = true;
                    return ingredient;
                });

                // pagination
                $scope.itemsPerPage = itemsPerPage;
                // when expiring ingredients changed, (re)load pagination
                $scope.$watch('expiringIngredients', function () {
                    // filter
                    var filteredRecipes =
                        $filter('recipesByExpiringIngredientsFilter')($scope.recipes, $scope.expiringIngredients);

                    // (re)load total items and current page
                    $scope.totalItems = filteredRecipes.length;
                    $scope.currentPage = 1;

                    // when page changed, reload paginated recipes
                    $scope.pageChanged = function () {
                        var start = ($scope.currentPage - 1) * $scope.itemsPerPage;
                        var end = start + $scope.itemsPerPage;
                        if (end >= filteredRecipes.length) {
                            end = filteredRecipes.length;
                        }
                        $scope.paginatedRecipes = filteredRecipes.slice(start, end);
                    };

                    // trigger paginated recipes
                    $scope.pageChanged();
                }, true)
            }, function (err) {
                $scope.loading = false;
                $scope.showFailureAlert(err);
            })
        }
    }, function (err) {
        $scope.loading = false;
        $scope.showFailureAlert(err);
    })
}]);

angular.module('liebessen-controllers').filter('recipesByExpiringIngredientsFilter', function () {
    return function (recipes, expiringIngredients) {
        if (!recipes || !expiringIngredients) {
            return []
        } else {
            var ingredientsIncluded = expiringIngredients.filter(function (ingredient) {
                return ingredient['selected'];
            }).map(function (ingredient) {
                return ingredient['name'];
            });

            return recipes.filter(function (recipe) {
                var ingredients = recipe['ingredients'];
                for (var i = 0 ; i < ingredients.length ; ++i) {
                    var ingredient = ingredients[i];
                    if (ingredientsIncluded.indexOf(ingredient) !== -1) {
                        return true;
                    }
                }
                return false;
            })
        }
    }
});