angular.module('liebessen-controllers').controller('CategoryController', ['$scope', '$routeParams', 'UserService', 'expirationDates', function ($scope, $routeParams, UserService, expirationDates) {

    (function () {
        $('#alert').bs3Alert({
            priority: 'success',
            titles: {
                success: '',
                danger: ''
            }
        });
    })();
    $scope.showSuccessAlert = function (msg) {
        $(document).trigger('show-alert', {
            priority: 'success',
            message: msg
        });
    };
    $scope.showFailureAlert = function (msg) {
        $(document).trigger('show-alert', {
            priority: 'danger',
            message: msg
        });
    };

    $scope.tempFood = {};
    $scope.timeNow = new Date();
    $scope.expiredFoods = [];
    $scope.empty = true;

    // grab the current category name
    $scope.currentCategory = $routeParams.category.toLowerCase();
    console.log('Hello from CategoryController: ' + $scope.currentCategory);
    $scope.categoryFilter = {'category' : $scope.currentCategory};

    $scope.predicate = 'expiresAt';
    $scope.reverse = true;
    $scope.order = function(predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

    /*
    Retrieve the foods of the category we are in
     */
    $scope.getFoods = function() {
        UserService.get().then(function (userData) {
                // success
                $scope.userData = userData;
                $scope.userFood = $scope.userData.ingredients;
                $scope.expiredFoods = [];

                for (var i = 0; i < $scope.userFood.length; i++) {
                    if($scope.userFood[i]['category'].toLowerCase() == $scope.currentCategory) {
                        $scope.empty = false;
                    }

                    if ($scope.userFood[i]['expiresAt'] < $scope.timeNow.toISOString() && $scope.userFood[i]['category'].toLowerCase() == $scope.currentCategory) {
                        $scope.expiredFoods.push($scope.userFood[i]);
                        $scope.hasExpired = true;
                        console.log($scope.expiredFoods);
                    }
                }
            },
            function (userData) {
                // error
            });
    }
    $scope.getFoods();



    /*
     * Method takes in a foodData object which contains the following
     * { name: String, category: String, quantity: Number, unit: String, expirationDate: Date }
     *
     * These will be stored in ng-model on the partial in a way similar to: foodData.name, foodData.category, etc
     * all of the components are required except for category, which will default to 'other'
     * *** should consider making 'unit' also optional, expiration date might default to 2weeks as well?
     */
    $scope.addFood = function(foodData) {
        foodData.category = $scope.currentCategory;
        // lol, key has first char as uppercase
        foodData.category = foodData.category.charAt(0).toUpperCase() + foodData.category.slice(1);

        if (foodData.name === '' || foodData.name === undefined) {
            $scope.showFailureAlert("Please add a name");
            return;
        }
        if (foodData.quantity === '' || foodData.quantity === undefined) {
            $scope.showFailureAlert("Please add a quantity");
            return;
        }

        if (foodData.expiresAt === '' || foodData.expiresAt === undefined) {
            console.log('no expires');
            var date = new Date();
            console.log(expirationDates);
            console.log(foodData.category);
            date.setDate(date.getDate() + expirationDates[foodData.category]*7);
            foodData.expiresAt = date;
        }

        // add the food to the curUser
        UserService.get().then(function(userData){
            console.log(userData);
            userData.ingredients.push(foodData);
            console.log(userData.ingredients);
            UserService.update({'ingredients':userData.ingredients}).then(function(res) {
                // success
                console.log('success adding food!');

                // reset fields
                $scope.tempFood = null;
                $scope.showSuccessAlert("Added food successfully");
                $scope.getFoods();
                // do something

            }, function(res) {
                // error
                $scope.showFailureAlert("Failed to add food successfully");
                // do something

            });
        }, function(data){
            // error
        });

    }

    $scope.deleteFood = function(foodData) {
        $scope.currentIngredientId = foodData._id;
        UserService.get().then(function (userData) {
            var curIngredient = $.grep($scope.userData.ingredients, function(elt){
                return elt._id === $scope.currentIngredientId;
            })[0];

            // since these are objects, can't user indexof
            // var index = userData.ingredients.indexOf(curIngredient);
            var index = -1;
            for (var i = 0; i < userData.ingredients.length; i++) {
                if ($scope.currentIngredientId === userData.ingredients[i]._id) {
                    index = i;
                }
            }
            var category = curIngredient.category;

            userData.ingredients.splice(index, 1);
            UserService.update({'ingredients' : userData.ingredients}).then(function() {
                // we should be redirected to the category page
                $scope.showSuccessAlert("Edit was successful");
                $scope.getFoods();
            }, function() {
                $scope.showFailureAlert("Edit failed");
            })

        })
    }
}]);