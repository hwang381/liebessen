angular.module('liebessen-controllers').controller('LoginController', ['$scope', 'UserService', '$location', function ($scope, UserService, $location) {
    $scope.form = {
        email: '',
        password: ''
    };

    // bs3Alert
    (function () {
        $('#alert').bs3Alert({
            priority: 'success',
            titles: {
                success: '',
                danger: ''
            }
        });
    })();
    $scope.showSuccessAlert = function (msg) {
        $(document).trigger('show-alert', {
            priority: 'success',
            message: msg
        });
    };
    $scope.showFailureAlert = function (msg) {
        $(document).trigger('show-alert', {
            priority: 'danger',
            message: msg
        });
    };

    // loading
    $scope.loading = false;

    $scope.submit = function () {
        // validation
        if (!$scope.form.email || $scope.form.email.indexOf('@') === -1) {
            $scope.showFailureAlert('Email is required');
            return;
        }
        if (!$scope.form.password) {
            $scope.showFailureAlert('Password is required');
            return;
        }

        // login
        $scope.loading = true;
        UserService.login($scope.form).then(function (data) {
            $location.path('/');
        }, function (err) {
            $scope.loading = false;
            $scope.showFailureAlert(err);
        })
    }
}]);