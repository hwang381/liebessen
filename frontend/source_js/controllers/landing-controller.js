angular.module('liebessen-controllers').controller('LandingController', ['$scope', function ($scope) {
    console.log('Hello from LandingController');
    //$("#main-nav").hide();

    /*Reference: http://stackoverflow.com/questions/5358183/is-it-possible-to-dynamically-scale-text-size-based-on-browser-width*/
    $(document ).ready( function() {
        var $body = $('body');

        var setBodyScale = function() {
            var scaleSource = $body.width(),
                scaleFactor = 0.35,
                maxScale = 600,
                minScale = 30;

            var taglineFontSize = scaleSource * scaleFactor;
            var buttonFontSize = scaleSource * 0.30;
            var buttonSize = scaleSource * 0.07;

            if (taglineFontSize > maxScale) taglineFontSize = maxScale;
            if (taglineFontSize < minScale) taglineFontSize = minScale;

            if (buttonFontSize > 200) buttonFontSize = 200;

            if (buttonSize > 30) buttonSize = 30;

            $('#tagline').css('font-size', taglineFontSize + '%');
            $('#signup-button').css('font-size', buttonFontSize + '%');
        };

        $(window).resize(function(){
            setBodyScale();
        });

        setBodyScale();
    });
}]);