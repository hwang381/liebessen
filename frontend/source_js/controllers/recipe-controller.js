angular.module('liebessen-controllers').controller('RecipeController', ['$scope', 'RecipeService', '$routeParams', 'UserService', function ($scope, RecipeService, $routeParams, UserService) {
    // bs3Alert
    (function () {
        $('#alert').bs3Alert({
            priority: 'success',
            titles: {
                success: '',
                danger: ''
            }
        });
    })();
    $scope.showSuccessAlert = function (msg) {
        $(document).trigger('show-alert', {
            priority: 'success',
            message: msg
        });
    };
    $scope.showFailureAlert = function (msg) {
        $(document).trigger('show-alert', {
            priority: 'danger',
            message: msg
        });
    };

    // loading
    $scope.loading = true;

    RecipeService.getDetail($routeParams['id']).then(function (recipe) {
        $scope.recipe = recipe;

        // if the recipe is favorites
        UserService.get().then(function (user) {
            var favoriteRecipes = user['favoriteRecipes'];
            var recipeId = $scope.recipe['recipe_id'];
            $scope.isInFavorites = favoriteRecipes.indexOf(recipeId) !== -1;
            $scope.loading = false;

            // when click on favorite
            $scope.onClickFavorite = function () {
                if ($scope.isInFavorites) {
                    UserService.removeFromFavoriteRecipes(recipeId).then(function (data) {
                        $scope.isInFavorites = false;
                    }, function (err) {
                        $scope.showFailureAlert(err);
                    })
                } else {
                    UserService.addToFavoriteRecipes(recipeId).then(function (data) {
                        $scope.isInFavorites = true;
                    }, function (err) {
                        $scope.showFailureAlert(err);
                    })
                }
            };
        }, function (err) {
            $scope.loading = false;
            $scope.showFailureAlert(err);
        });
    }, function (err) {
        $scope.loading = false;
        $scope.showFailureAlert(err);
    })
}]);