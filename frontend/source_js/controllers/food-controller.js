angular.module('liebessen-controllers').controller('FoodController', ['$scope', '$routeParams', '$http', 'FoodService', 'UserService', 'expirationDates', 'funFacts', function ($scope, $routeParams, $http, FoodService, UserService, expirationDates, funFacts) {
    console.log('Hello from FoodController');

    (function () {
        $('#alert').bs3Alert({
            priority: 'success',
            titles: {
                success: '',
                danger: ''
            }
        });
    })();
    $scope.showSuccessAlert = function (msg) {
        $(document).trigger('show-alert', {
            priority: 'success',
            message: msg
        });
    };
    $scope.showFailureAlert = function (msg) {
        $(document).trigger('show-alert', {
            priority: 'danger',
            message: msg
        });
    };

    $scope.predicate = 'expiresAt';
    $scope.reverse = true;
    $scope.order = function(predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

    $scope.empty = false;

    $scope.userid = $routeParams.id;
    $scope.userFood = {};
    $scope.tempFood = {};
    $scope.timeNow = new Date();
    $scope.expiredFoods = [];
    $scope.searchQuery = '';

    $scope.funFact = funFacts[Math.floor(Math.random()*funFacts.length)];

    $scope.categories = [
        {category:'Beverages', image_url:'./public/media/categories/Different_beverages.jpeg'},
        {category:'Breads', image_url:'./public/media/categories/339410.jpg'},
        {category:'Condiments', image_url:'./public/media/categories//shutterstock_182370455.jpg'},
        {category:'Dairy', image_url:'./public/media/categories//milk-and-acne.jpg'},
        {category:'Dry', image_url:'./public/media/categories//dry-ingredients.jpg'},
        {category:'Frozen', image_url:'./public/media/categories//billboard.jpg'},
        {category:'Meat', image_url:'./public/media/categories//6_types-of-meats.jpg'},
        {category:'Preserves', image_url:'./public/media/categories//mamaspreserves_welcome_jams7.jpg'},
        {category:'Produce', image_url:'./public/media/categories//fruits-and-vegetables-wallpaper-2.jpg'},
        {category:'Other', image_url:'./public/media/categories//1375930515977128357Coloured%20question%20marks.svg.med.png'}
    ];



    $scope.$watch('searchQuery', function() {
        if ($scope.searchQuery !== '') $scope.showList = true;
        $scope.searchFilter = $scope.searchQuery;
    });


    /*
    * Method takes the current user and loads in information about the current user
    * and store it in $scope.userData
    * We will use this data to display all the information about the user on each page that needs it
    */
    $scope.getFoods = function() {
        // everything in here depends on how user is stored, but for now, userData will just take everything that
        // the request returns
        UserService.get().then(function(userData) {
            $scope.userFood = userData.ingredients;

            if ($scope.userFood.length == 0) $scope.empty = true;
            else $scope.empty = false;

            $scope.expiredFoods = [];
            for (var i = 0; i < $scope.userFood.length; i++) {
                if ($scope.userFood[i]['expiresAt'] < $scope.timeNow.toISOString()) {
                    $scope.expiredFoods.push($scope.userFood[i]);
                    $scope.hasExpired = true;
                    console.log($scope.expiredFoods);
                }
            }
        });
    }
    $scope.getFoods();



    /*
    * Method takes in a foodData object which contains the following
    * { name: String, category: String, quantity: Number, unit: String, expirationDate: Date }
    *
    * These will be stored in ng-model on the partial in a way similar to: foodData.name, foodData.category, etc
    * all of the components are required except for category, which will default to 'other'
    * *** should consider making 'unit' also optional, expiration date might default to 2weeks as well?
    */
    $scope.addFood = function(foodData) {
        if (foodData.name === '' || foodData.name === undefined) {
            $scope.showFailureAlert("Please add a name");
            return;
        }
        if (foodData.quantity === '' || foodData.quantity === undefined) {
            $scope.showFailureAlert("Please add a quantity");
            return;
        }
        if (foodData.category === '' || foodData.category === undefined) {
          foodData.category = 'Other';
        }
        if (foodData.expiresAt === '' || foodData.expiresAt === undefined) {
            var date = new Date();
            date.setDate(date.getDate() + expirationDates[foodData.category]*7);
            foodData.expiresAt = date;
        }

        console.log(foodData);

        // add the food to the curUser
        UserService.get().then(function(userData){
            console.log(userData);
            userData.ingredients.push(foodData);
            console.log(userData.ingredients);
            UserService.update({'ingredients':userData.ingredients}).then(function(res) {
                // success
                $scope.showSuccessAlert("Added food successfully");

                // empty the fields
                $scope.tempFood = null;
                $scope.getFoods();

            }, function(res) {
                // error
                $scope.showFailureAlert("Failed to add food successfully");

                // do something

            });
        }, function(data){
      // error
        });

    }

    $scope.deleteFood = function(foodData) {
        $scope.currentIngredientId = foodData._id;
        UserService.get().then(function (userData) {
            var curIngredient = $.grep(userData.ingredients, function(elt){
                return elt._id === $scope.currentIngredientId;
            })[0];

            // since these are objects, can't user indexof
            // var index = userData.ingredients.indexOf(curIngredient);
            var index = -1;
            for (var i = 0; i < userData.ingredients.length; i++) {
                if ($scope.currentIngredientId === userData.ingredients[i]._id) {
                    index = i;
                }
            }
            var category = curIngredient.category;

            userData.ingredients.splice(index, 1);
            UserService.update({'ingredients' : userData.ingredients}).then(function() {
                // we should be redirected to the category page
                $scope.showSuccessAlert("Edit was successful");
                $scope.getFoods();
            }, function() {
                $scope.showFailureAlert("Edit failed");
            })

        })
    }

    /*
    * Method for making an API call to food2fork for a certain ingredient
    * this will populate a recipe field for a particular food ingredient
    * we are looking at
    *
    * Params:
    *   ingredient - what we are querying for in the food2fork api
    *   n          - how many top recipes we want to retrieve (max: 30)
    */
    $scope.getRelatedRecipes = function(ingredient, n) {
    // we want to get the top n hits for recipes given the ingredient we are on
    // defaults to 30 if n is undefined
    var n = typeof n !== 'undefined' ?  n : 30;
    if (ingredient === 'undefined' || ingredient === '') {
        console.log('empty search query');
        return [];
    }
    $scope.relatedRecipes = [];


    // make API call to the service and retrieve the top hits for the ingredient
    FoodService.getRelatedRecipes(ingredient)
        .then(function(recipeData) {
            // success
            console.log(recipeData);
            // slice to get the top recipes from 0 to index n
            $scope.relatedRecipes = recipeData.recipes.slice(0, n);

        }, function(err) {
            // error
            console.log('no recipes found');
            console.log(err);
        });
    };

    $scope.listBtnClicked = false;
}]);