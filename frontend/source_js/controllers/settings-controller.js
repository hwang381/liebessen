angular.module('liebessen-controllers').controller('SettingsController', ['$scope', 'UserService', '$location', function ($scope, UserService, $location) {
    $scope.logout = function () {
        UserService.logout().then(function (data) {
            $location.path('/landing');
        }, function (err) {
            console.error(err);
        })
    }
}]);