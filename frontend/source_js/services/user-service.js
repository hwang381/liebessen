angular.module('liebessen-services').factory('D', ['$q', function ($q) {
    return {
        defer: function (promise, resolveCallback, rejectCallback) {
            var deferred = $q.defer();

            promise.then(function (response) {
                var payload = response.data;
                if (payload['data']) {
                    deferred.resolve(payload['data']);
                    if (resolveCallback) {
                        resolveCallback();
                    }
                } else {
                    deferred.reject(payload['err']['message'] ? payload['err']['message'] : payload['err']);
                    if (rejectCallback) {
                        rejectCallback();
                    }
                }
            }, function (response) {
                var payload = response.data;
                deferred.reject(payload['err']['message'] ? payload['err']['message'] : payload['err']);
                if (rejectCallback) {
                    rejectCallback();
                }
            });

            return deferred.promise;
        }
    }
}]);

angular.module('liebessen-services').factory('UserService', ['$http', '$q', 'D', 'RecipeService', function ($http, $q, D, RecipeService) {
    var loggedIn = false;
    return {
        isLoggedIn: function () {
            return loggedIn;
        },
        setToLoggedIn: function () {
            loggedIn = true;
        },
        setToNotLoggedIn: function () {
            loggedIn = false;
        },
        signup: function (form) {
            return D.defer($http.post('/api/users/signup', form));
        },
        login: function (form) {
            return D.defer($http.post('/api/users/login', form), this.setToLoggedIn);
        },
        logout: function () {
            return D.defer($http.get('/api/users/logout'), this.setToNotLoggedIn);
        },
        status: function () {
            return D.defer($http.get('/api/users/status'));
        },
        get: function () {
            return D.defer($http.get('/api/users'));
        },
        update: function (newUser) {
            return D.defer($http.put('/api/users', newUser));
        },
        addToFavoriteRecipes: function (recipeId) {
            var context = this;
            return this.get()
                .then(function (user) {
                    // get favorite recipes and add to it
                    var favoriteRecipes = user['favoriteRecipes'];
                    favoriteRecipes.push(recipeId);

                    // update
                    return context.update({favoriteRecipes: favoriteRecipes});
                })
        },
        removeFromFavoriteRecipes: function (recipeId) {
            var context = this;
            return this.get()
                .then(function (user) {
                    // get favorite recipes and remove from it
                    var favoriteRecipes = user['favoriteRecipes'];
                    var index = favoriteRecipes.indexOf(recipeId);
                    if (index !== -1) {
                        favoriteRecipes.splice(index, 1);
                    }

                    // update
                    return context.update({favoriteRecipes: favoriteRecipes});
                })
        },
        getFavoriteRecipes: function () {
            return this.get()
                .then(function (user) {
                    var favoriteRecipes = user['favoriteRecipes'];
                    return $q.all(favoriteRecipes.map(function (id) {
                        return RecipeService.getDetail(id);
                    }));
                });
        }
    }
}]);