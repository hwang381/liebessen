angular.module('liebessen-services').factory('RecipeService', ['$http', 'D', '$q', function ($http, D, $q) {
    return {
        get: function (ingredients) {
            return D.defer($http.get('/api/recipes', {
                params: {
                    'ingredients[]': ingredients
                }
            })).then(function (recipes) {
                return recipes.map(function (recipe) {
                    recipe['ingredients'] = ingredients;
                    return recipe;
                });
            })
        },
        getMultiple: function (ingredientsList) {
            var context = this;
            return $q.all(ingredientsList.map(function (ingredients) {
                return context.get(ingredients);
            })).then(function (recipes) {
                return recipes.reduce(function (prev, cur) {
                    return prev.concat(cur);
                }, []);
            });
        },
        getDetail: function (id) {
            return D.defer($http.get('/api/recipes/' + id));
        }
    }
}]);