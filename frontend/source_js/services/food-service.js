angular.module('liebessen-services').factory('FoodService', ['$http', function ($http) {
    return {
        getRelatedRecipes: function (ingredient) {
            // eventually should move apiKey and this request to backend so we don't have the key public
            var apiKey = 'e13991b65d6cce322896341218cbb418';
            // var baseUrl = 'https://community-food2fork.p.mashape.com/search?key=' + apiKey + '&q=' + ingredient;
            var baseUrl = 'http://food2fork.com/api/search?key=' + apiKey + '&q=' + ingredient;

            return $http({
                method: 'JSONP',
                url: baseUrl,
                params: {
                    format: 'jsonp',
                    json_callback: 'JSON_CALLBACK'
                }
            });
        }
    }
}]);