angular.module('liebessen-services').factory('FirstTimeService', function () {
    var KEY = 'libessen.firstTime';
    return {
        isFirstTime: function () {
            return !localStorage.getItem(KEY)
        },
        setNotFirstTime: function () {
            localStorage.setItem(KEY, false);
        }
    }
});