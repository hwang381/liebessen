var liebessen = angular.module('liebessen', ['ngRoute', 'liebessen-controllers', 'liebessen-services', 'ui.bootstrap']);

liebessen.config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
    $routeProvider
        .when('/landing', {
            templateUrl: '/public/partials/landing.html',
            controller: 'LandingController',
            hideNav: true
        })
        .when('/login', {
            templateUrl: '/public/partials/login.html',
            controller: 'LoginController'
        })
        .when('/signup', {
            templateUrl: '/public/partials/signup.html',
            controller: 'SignupController'
        })
        .when('/food', {
            templateUrl: '/public/partials/food.html',
            controller: 'FoodController',
            protectedRoute: true
        })
        .when('/food/:category', {
            templateUrl: '/public/partials/category.html',
            controller: 'CategoryController',
            protectedRoute: true
        })
        .when('/food/:category/:id', {
            templateUrl: '/public/partials/ingredient.html',
            controller: 'IngredientController',
            protectedRoute: true
        })
        .when('/recipes', {
            templateUrl: '/public/partials/recipes.html',
            controller: 'RecipesController',
            protectedRoute: true
        })
        .when('/fav-recipes', {
            templateUrl: '/public/partials/fav-recipes.html',
            controller: 'FavRecipesController',
            protectedRoute: true
        })
        .when('/recipe/:id', {
            templateUrl: '/public/partials/recipe.html',
            controller: 'RecipeController',
            protectedRoute: true
        })
        .when('/settings', {
            templateUrl: '/public/partials/settings.html',
            controller: 'SettingsController',
            protectedRoute: true
        })
        .otherwise({
            redirectTo: '/food'
        });

    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]);

liebessen.run(['$rootScope', '$location', 'FirstTimeService', 'UserService', '$route', function ($rootScope, $location, FirstTimeService, UserService, $route) {
    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        var path = next['$$route'] ? next['$$route']['originalPath'] : '/food';
        var protectedRoute = next['$$route'] ? next['$$route']['protectedRoute'] : true;
        var hideNav = next['$$route'] ? next['$$route']['hideNav'] : false;

        // show or hide nav
        if (hideNav) {
            $("#main-nav").hide();
        } else {
            $("#main-nav").show();
        }

        // toggle nav buttons
        function toggleNavbarButtons(loggedIn) {
            if (loggedIn) {
                $('.not_logged_in').hide();
                $('.logged_in').show();
            } else {
                $('.not_logged_in').show();
                $('.logged_in').hide();
            }
        }

        if (FirstTimeService.isFirstTime() && path !== '/landing') {
            // if first time and not routing to landing, route to landing
            $location.path('/landing');
            FirstTimeService.setNotFirstTime();
        } else {
            // if routing to protected route and not routing to login,
            if (protectedRoute && path !== '/login') {
                UserService.status().then(function (data) {
                    // if is logged in, show corresponding buttons
                    toggleNavbarButtons(true);
                }, function (error) {
                    // if not logged in, show corresponding buttons, and re-route to login
                    toggleNavbarButtons(false);
                    $location.path('login');
                    $route.reload();
                })
            } else {
                toggleNavbarButtons(false);
            }
        }
    });
}]);

liebessen.constant("expirationDates", {
    Beverages: 2,
    Breads: 1,
    Condiments: 26,
    Dairy: 1,
    Frozen: 52,
    Meat: 1,
    Preserves: 52,
    Produce: 1,
    Other: 2
});

liebessen.constant("funFacts", ["A survey showed 29% of adults say they have been splashed or scalded by hot drinks while dunking biscuits.",
    "Ortharexia Nervosa is an eating disorder where the sufferer is obsessed with eating healthy food.",
    "Ketchup was sold in the 1830's as medicine.",
    "Dry swallowing one teaspoon of sugar can commonly cure hic-ups.",
    "In Kentucky, it is illegal to carry an ice cream cone in your back pocket.",
    "The fear of cooking is known as Mageirocophobia and is a recognised phobia.",
    "The tea bag was introduced in 1908 by Thomas Sullivan of New York.",
    "The tall chef's hat is called a toque.",
    "Arachibutyrophobia is the fear of peanut butter sticking to the roof of the mouth.",
    "In South Africa, termites and ants are often roasted and eaten by the handful, like popcorn.",
    "Every time you lick a stamp, you consume 1/10 of a calorie.",
    "Pearls melt in vinegar.",
    "The '57' on the Heinz ketchup bottle represents the number of pickle types the company once had.",
    "Marmite was first introduced into the UK in 1902.",
    "The ancient Greeks chewed a gum-like substance called mastic that came from the bark of a tree.",
    "Honeybee workers must visit 2 million flowers to make one pound of honey.",
    "The fear of vegetables is called Lachanophobia.",
    "Almonds are a member of the peach family.",
    "If you boil beetroot in water, and then massage the water into your scalp each night, it works as an effective cure for dandruff.",
    "In the United States, lettuce is the second most popular fresh vegetable.",
    "Grape growing is the largest food industry in the world as there are more than 60 species and 8000 varieties of grapes.",
    "The average person eats eight pounds of grapes each year.",
    "There are more than 7,000 varieties of apples grown in the world.",
    "A cluster of bananas id formerly called a Ôhand'. Along that theme, a single banana is called a Ôfinger'.",
    "Onion is Latin for Ôlarge pearl'.",
    "Apples, pears, cherries and strawberries are all members of the rose family.",
    "The word vegetable has no scientific definition, so it's still acceptable to call a tomato a vegetable.",
    "In the Philippines, it is considered good luck if a coconut is cleanly split open without jagged edges.",
    "When cranberries are ripe, they bounce like a rubber ball.",
    "The most amount of grapes eaten in 3 minutes is 133. This record was set in 2001 by Mat Hand, from the UK.",
    "There are more than 10,000 varieties of tomatoes.",
    "In 2001, there were more than 300 banana-related accidents in Britain, most involving people slipping on skins.",
    "If you want to speed up the ripening of a pineapple, so that you can eat it faster, then you can do it by standing it upside down (on the leafy end).",
    "Carrots have zero fat content.",
    "Peanuts can be used to make dynamite.",
    "A watermelon is over 92% water by weight.",
    "Alliumphobia is the fear of garlic.",
    "A row of corn always has an even number.",
    "The pumpkin originated in Mexico about 9,000 years ago.",
    "Square watermelons sell for about $85.",
    "Turnips are high in fibre, Vitamin C, Calcium and Potassium.",
    "Eating a lot of beetroot turns your pee into a pink colour.",
    "Biting a wooden spoon whilst chopping an onion will stop your eyes from watering.",
    "Each pineapple plant only produces just one pineapple per year.",
    "Lettuce is a member of the sunflower family.",
    "Blueberries are a good source of Vitamin C and fibre.",
    "Pumpkin flowers are edible.",
    "Peaches are the third most popular fruit grown in America.",
    "We are eating 900% more broccoli than we did 20 years ago.",
    "There are more than 600 pasta shapes produced worldwide.",
    "Geomelophagia is someone who has the urge to eat raw potatoes.",
    "9th August is officially National Rice Pudding Day.",
    "Rice can be used in beer, dog food, baby food, breakfast cereals, snacks, frozen foods and sauces!",
    "October is National Pasta Month.",
    "Canadian neurosurgeon Dr. Wilder Penfield, while operating on epilepsy patients, discovered the ÔToast Centre' of the human brain, which is wholly dedicated to detecting when toast is burning!",
    "16 billion jelly beans are made for Easter. stacked end to end, the jelly beans would circle the globe nearly 3 times!",
    "Before 1991 Twix Bars were internationally knows as ÔRaider'.",
    "The Arabs invented caramel.",
    "Before Walkers owned Wotsits, they sold a rival known as Cheetos on the UK market.",
    "Pringles were first sold in America in 1968 but were not popular until the mid 1970's.",
    "Saddam Hussein liked Bounty Bars!",
    "Frank Mars invented the Snickers chocolate bar. He named it Snickers after his favourite horse.",
    "The M's in M & M's stand for ÔMars & Murrie', the co-creators of the candy.",
    "Twinkies originally had banana flavoured filling, but switched to vanilla when World War 2 bought the banana trade to a halt.",
    "Cadbury's Cream Eggs first went on sale in 1971.",
    "In 2006, Shari's Berries pioneered the concept of delivering chocolate dipped fruits nationwide in freeze packs.",
    "The Kit Kat was originally made by Rowntree Limited, until 1988 when they were bought out by Nestle.",
    "Juicy Fruit and Wrigley's Spearmint gums are more than 100 years old!",
    "Hundreds and Thousands (Sprinkles, Nonpareils, Jimmies) are small round balls of brightly coloured sugar used as decorations on cakes, cookies, trifles and other desserts. Their use dates back at least to the early 19th century.",
    "Gummy Bears are only 79 millimetres long in length.",
    "Chocolate can kill dogs; it directly affects their heart and nervous system.",
    "Deep fried chocolate bars contain about 850 calories per bar.",
    "The world's oldest chocolates are 106 years old. A tin of chocolates from the coronation of King Edward VII from 1902.",
    "Yorkie Chocolate Bars are not for girls!",
    "Sugar is the only taste that humans are born craving.",
    "The Swiss eat the most chocolate, followed by the English.",
    "The Bourbon biscuit was introduced in 1910 originally under the name Creola.",
    "In 15th century France, chocolate could only be eaten by members of the royal court.",
    "Gorgonzola cheese dates back to the year 879!",
    "Many mass-produced ice creams have seaweed in them.",
    "Cheese products contain less than 51% cheese.",
    "Some yoghurt contain beef or pork gelatin.",
    "The weight of a Babybel is 21 grammes.",
    "An average American will eat the equivalent of 28 pigs in their lifetime.",
    "National Pigs-in-a-Blanket Day is celebrated on 24th April every year.",
    "The first soup was made from hippopotamus and dates back to 6000 B.C.",
    "Baked beans are low in fat and have a lot of fibre and protein.",
    "Worcestershire sauce is made from dissolved fish.",
    "Pescetarians are vegetarians who eat fish.",
    "The largest item on any menu in the world is the roast camel.",
    "Egg yolks are one of the few foods that naturally contain Vitamin D.",
    "The average hot dog is consumed in 6 bites.",
    "Until well into the sixteenth century, bacon was a Middle English term used to refer to all pork in general.",
    "In America, anchovies always rank last on the list of favourite toppings.",
    "The world average of the amount of meat eaten per year is: 173 lbs per person.",
    "French fries came from Belgium, but are most popular in the US.",
    "KFC (Kentucky Fried Chicken) was founded by Colonel Harland Sanders in 1952.",
    "We eat 300 million portions of fish and chips in Britain each year.",
    "Chicken McNuggets contain beef additives."])

liebessen.constant("itemsPerPage", 12);

angular.module('liebessen-controllers', []);
angular.module('liebessen-services', []);
